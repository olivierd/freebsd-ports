# Created by: Henry Hu <henry.hu.sh@gmail.com>
# $FreeBSD: head/textproc/ibus/Makefile 564238 2021-02-06 19:10:39Z bapt $

PORTNAME=	ibus
PORTVERSION=	1.5.24
CATEGORIES=	textproc

MAINTAINER=	henry.hu.sh@gmail.com
COMMENT=	Intelligent Input Bus for Linux / Unix OS

LICENSE=	LGPL21
LICENSE_FILE=	${WRKSRC}/COPYING

BUILD_DEPENDS=	${PYTHON_PKGNAMEPREFIX}dbus>=0.83.0:devel/py-dbus@${PY_FLAVOR} \
		${LOCALBASE}/share/unicode/ucd/NamesList.txt:textproc/UCD \
		${LOCALBASE}/libdata/pkgconfig/iso-codes.pc:misc/iso-codes \
		gtkdocize:textproc/gtk-doc \
		bash:shells/bash
LIB_DEPENDS=	libfontconfig.so:x11-fonts/fontconfig \
		libfreetype.so:print/freetype2 \
		libdbus-1.so:devel/dbus \
		libnotify.so:devel/libnotify
RUN_DEPENDS=	${PYTHON_PKGNAMEPREFIX}dbus>=0.83.0:devel/py-dbus@${PY_FLAVOR} \
		${LOCALBASE}/share/xml/iso-codes/iso_639.xml:misc/iso-codes \
		setxkbmap:x11/setxkbmap \
		xkeyboard-config>0:x11/xkeyboard-config

USES=		autoreconf compiler cpe gmake gnome libtool localbase \
		pathfix pkgconfig python:3.5+ shebangfix xorg
USE_GNOME=	cairo glib20 gtk30 dconf intltool librsvg2 pygobject3 \
		introspection:build
USE_LDCONFIG=	yes
USE_PYTHON=	py3kplist
USE_XORG=	x11 xi xtst

MAKE_JOBS_UNSAFE=yes

SHEBANG_GLOB=	*.sh

USE_GITHUB=	yes

GNU_CONFIGURE=	yes
INSTALLS_ICONS=	yes
OPTIONS_SUB=	yes
INSTALL_TARGET=	install-strip
CONFIGURE_ARGS=	--without-html-dir \
		--with-ucd-dir=${LOCALBASE}/share/unicode/ucd \
		--disable-python2 --disable-python-library \
		--disable-gtk2 \
		--enable-introspection=yes

OPTIONS_DEFINE=	EMOJI ENGINE NLS VALA WAYLAND XIM
OPTIONS_DEFAULT=	EMOJI ENGINE VALA WAYLAND XIM

VALA_DESC=	Install Vala binding
XIM_DESC=	Install XIM server
ENGINE_DESC=	Install ibus simple engine
EMOJI_DESC=	Install emoji dictionary
CPE_VENDOR=	ibus_project

NLS_USES=		gettext
NLS_USES_OFF=		gettext-tools
NLS_CONFIGURE_ENABLE=	nls

VALA_BUILD_DEPENDS=	vala>=0.20:lang/vala
VALA_CONFIGURE_ENABLE=	vala

XIM_CONFIGURE_ENABLE=	xim

ENGINE_CONFIGURE_ENABLE=	engine

EMOJIONE_TAG=		ba845a7e24aac26cf3cf22abc19bea215d94fbf3 # 2.2.7
EMOJI_CONFIGURE_ON=	--with-unicode-emoji-dir=${LOCALBASE}/share/unicode/emoji \
			--with-emoji-annotation-dir=${LOCALBASE}/share/unicode/cldr/common/annotations
EMOJI_CONFIGURE_ENABLE=	emoji-dict
EMOJI_BUILD_DEPENDS=	json-glib>=0:devel/json-glib \
			unicode-emoji>0:misc/unicode-emoji \
			cldr-emoji-annotation>0:misc/cldr-emoji-annotation

WAYLAND_LIB_DEPENDS=	libxkbcommon.so:x11/libxkbcommon \
			libwayland-client.so:graphics/wayland
WAYLAND_CONFIGURE_ENABLE=	wayland

GLIB_SCHEMAS=org.freedesktop.ibus.gschema.xml

.include <bsd.port.pre.mk>

.if ${COMPILER_TYPE} == gcc && ${COMPILER_VERSION} < 46
USE_GCC=	yes
.endif

.if ${PORT_OPTIONS:MENGINE}
PLIST_SUB+=	COMPDIR=""
.else
PLIST_SUB+=	COMPDIR="@comment "
.endif

pre-configure:
	cd ${CONFIGURE_WRKSRC} && gtkdocize --copy --flavour no-tmpl

post-install:
	${MKDIR} ${STAGEDIR}${PREFIX}/etc/xdg/autostart
	${INSTALL_DATA} ${PATCHDIR}/ibus.desktop ${STAGEDIR}${PREFIX}/etc/xdg/autostart

do-test:
	${MAKE} -C ${WRKSRC}/src/tests check

.include <bsd.port.post.mk>
